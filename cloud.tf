locals {
  cloud_repos             = var.has_cloud ? var.cloud : []
  cloud_group_path        = var.cloud_group_path != "" ? var.cloud_group_path : var.cloud_group_name
  cloud_group_description = var.cloud_group_description != "" ? var.cloud_group_description : "Cloud"
  cloud_unique_group_count = var.has_cloud ? var.cloud_unique_group_count : 0
}

resource "gitlab_group" "cloud" {
  count = var.has_cloud ? 1 : 0

  name = "cloud"
  path = "cloud"
  description = "Cloud"

  parent_id = gitlab_group.group.id

  visibility_level = var.gitlab_visibility_level
}

module "cloud" {
  source = "git::ssh://git@gitlab.com/open-source-devex/terraform-modules/gitlab/projects-group.git?ref=v2.0.7"

  parent_group_id = var.has_cloud ? gitlab_group.cloud[0].id : 0

  visibility_level = var.gitlab_visibility_level

  project_merge_method = var.gitlab_project_merge_method

  groups = local.cloud_repos

  unique_group_count = local.cloud_unique_group_count

  cicd_project_path = var.cicd_project_path

  install_cicd_deploy_key = var.install_cicd_deploy_key
  cicd_deploy_key = var.cicd_deploy_key
  cicd_deploy_key_name = var.cicd_deploy_key_name
}
