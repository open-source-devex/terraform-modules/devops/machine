# git-cloud

Terraform module to create a git centric platform for building systems in the cloud

## Usage

See the examples directory for how to use the module.
To run the examples you need a `gitlab_token` of a user with access to the open-source-devex project, or you need to adapt the parent group ids in the examples before running them.
An easy way to provide the token is to create a _overrides_ file in the example directory:
```hcl
# secrets_override.tf
variable gitlab_token {
  default = "...."
}
```

In CI builds this value comes from a group variable.
