terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-devops-machine-minimal"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../../"

  parent_group_id = "5785002" # example-implementation/testing/sub-group-1

  gitlab_visibility_level = "private"

  group_name        = "group-name"
  group_path        = "group-path"
  group_description = "Group Description"
}
