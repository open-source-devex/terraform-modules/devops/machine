terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-devops-machine-complete"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

locals {
  parent_group = "5752175"
}

module "test" {
  source = "../../"

  parent_group_id = "5785002" # example-implementation/testing/sub-group-1

  gitlab_visibility_level = "private"

  gitlab_project_merge_method = "rebase_merge"

  group_name        = "group-name"
  group_path        = "group-path"
  group_description = "Group Description"

  has_cloud        = true
  has_applications = true

  cicd_project_path = "0-cicd"

  install_cicd_deploy_key = true
  cicd_deploy_key         = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDE5COWbGExCQGQq4v4LI4jE/iJyrwNS8qOT8NcAwoOGDlpTG6qpMhDdyduOofSUT/2r7eyfBK3UsvgnPxqcgeqYvYpVlQQPbmYZ50kgpZam/sulfnwptrHhPfm/KBdN9LzrYmpwE4gKUrvfOU7+MeYalVEQthi2viJq1SZCv7tmw=="
  cicd_deploy_key_name    = "Key used in testing of TF modules"

  create_clone_machine_script = true
}

output "repository_clone_cmd" {
  value = module.test.repository_clone_cmd
}
