locals {
  applications                    = var.has_applications ? var.applications : []
  applications_group_path         = var.applications_group_path != "" ? var.applications_group_path : var.applications_group_name
  applications_group_description  = var.applications_group_description != "" ? var.applications_group_description : "Applications"
  applications_unique_group_count = var.has_applications ? var.applications_unique_group_count : 0

}

resource "gitlab_group" "applications" {
  count = var.has_applications ? 1 : 0

  name        = "applications"
  path        = "applications"
  description = "Applications"

  parent_id = gitlab_group.group.id

  visibility_level = var.gitlab_visibility_level
}

module "applications" {
  source = "git::ssh://git@gitlab.com/open-source-devex/terraform-modules/gitlab/projects-group.git?ref=v2.0.7"

  parent_group_id = var.has_applications ? gitlab_group.applications[0].id : 0

  visibility_level = var.gitlab_visibility_level

  project_merge_method = var.gitlab_project_merge_method

  groups = local.applications

  unique_group_count = local.applications_unique_group_count

  cicd_project_path = var.cicd_project_path

  install_cicd_deploy_key = var.install_cicd_deploy_key
  cicd_deploy_key = var.cicd_deploy_key
  cicd_deploy_key_name = var.cicd_deploy_key_name
}
