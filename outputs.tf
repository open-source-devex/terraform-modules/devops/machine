locals {
  cloud_clone_cmds = [
    "echo \"==> Cloning cloud repositories\"",
    "mkdir ${gitlab_group.cloud[0].path};",
    "cd ${gitlab_group.cloud[0].path};",
    module.cloud.repository_clone_cmd,
    "cd ..;",
  ]

  cloud_repository_clone_cmd = var.has_cloud ? join("\n", local.cloud_clone_cmds) : ""

  applications_clone_cmds = [
    "echo \"==> Cloning applications repositories\"",
    "mkdir ${gitlab_group.applications[0].path};",
    "cd ${gitlab_group.applications[0].path};",
    module.applications.repository_clone_cmd,
    "cd ..;",
  ]

  applications_repository_clone_cmd = var.has_applications ? join("\n", local.applications_clone_cmds) : ""

  clone_all_cmd = join("\n", [local.cloud_repository_clone_cmd, "\n", local.applications_repository_clone_cmd])
}

output "repository_clone_cmd" {
  value = local.clone_all_cmd
}
