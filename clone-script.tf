module "clone_script" {
  source = "git::ssh://git@gitlab.com/open-source-devex/terraform-modules/devops/script.git?ref=v1.0.0"

  create_script = var.create_clone_machine_script

  script_name          = var.clone_script_name
  script_template_file = "${path.module}/files/templates/clone-machine.sh.tpl"
  script_template_vars = {
    git_clone_repos = local.clone_all_cmd
    machine_name    = var.group_name
  }
}
