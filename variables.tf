variable parent_group_id {
  type = string
}

variable group_name {
  type = string
}

variable group_path {
  type    = string
  default = ""
}

variable group_description {
  type    = string
  default = ""
}

variable gitlab_visibility_level {
  type    = string
  default = "public"
}

variable gitlab_project_merge_method {
  default = "rebase_merge"
}

variable "has_cloud" {
  type    = bool
  default = false
}

variable "cloud_group_name" {
  default = "cloud"
}

variable "cloud_group_path" {
  type    = string
  default = ""
}

variable cloud_group_description {
  type    = string
  default = ""
}

variable cloud_unique_group_count {
  type        = number
  default     = 3
  description = "Set this variable to the count of unique groups for cloud."
}

variable cloud {
  type = list(object({
    name        = string,
    path        = string,
    description = string
    projects    = list(object({
      name                       = string,
      path                       = string,
      description                = string,
      kind                       = string,
      container_registry_enabled = bool,
      issues_enabled             = bool,
      wiki_enabled               = bool,
      snippets_enabled           = bool,
    }))
  }))

  default = [
    {
      name        = "infrastructure"
      path        = "infrastructure"
      description = "Repositories for cloud infrastructure layers"
      projects    = [
        {
          name                       = "1-root"
          path                       = "1-root"
          description                = "Infrastructure root, top-level and global resources go here."
          kind                       = "terraform"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
        {
          name                       = "2-network"
          path                       = "2-network"
          description                = "Network layer"
          kind                       = "terraform"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
      ]
    },
    {
      name        = "templates"
      path        = "templates"
      description = "Repositories for machine templates",
      projects    = [
        {
          name                       = "1-baseline"
          path                       = "1-baseline"
          description                = "Baseline for all templates"
          kind                       = "packer"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
      ]
    },
    {
      name        = "containers"
      path        = "containers"
      description = "Repositories for containers",
      projects    = [
        {
          name                       = "1-baseline"
          path                       = "1-baseline"
          description                = "Baseline for all containers"
          kind                       = "docker"
          container_registry_enabled = true,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
      ]
    },
  ]
}

variable has_applications {
  type    = bool
  default = false
}

variable applications_group_name {
  default = "applications"
}

variable applications_group_path {
  type    = string
  default = ""
}

variable applications_group_description {
  type    = string
  default = ""
}

variable applications_unique_group_count {
  type        = number
  default     = 2
  description = "Set this variable to the count of unique groups for applications."
}

variable applications {
  type = list(object({
    name        = string,
    path        = string,
    description = string
    projects    = list(object({
      name                       = string,
      path                       = string,
      description                = string,
      kind                       = string,
      container_registry_enabled = bool,
      issues_enabled             = bool,
      wiki_enabled               = bool,
      snippets_enabled           = bool,
    }))
  }))

  default = [
    {
      name        = "applications"
      path        = "applications"
      description = "Repositories for applications"
      projects    = [
        {
          name                       = "1-my-service"
          path                       = "1-my-service"
          description                = "Example service project"
          kind                       = ""
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
        {
          name                       = "2-my-function"
          path                       = "2-my-function"
          description                = "Example function project"
          kind                       = ""
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
      ]
    },
    {
      name        = "containers"
      path        = "containers"
      description = "Repositories for containers",
      projects    = [
        {
          name                       = "1-baseline"
          path                       = "1-baseline"
          description                = "Baseline for all containers"
          kind                       = "docker"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
      ]
    },
  ]
}

variable cicd_project_path {
  type    = string
  default = ""
}

variable install_cicd_deploy_key {
  type    = bool
  default = false
}

variable "cicd_deploy_key_name" {
  type    = string
  default = ""
}

variable "cicd_deploy_key" {
  type    = string
  default = ""
}

variable "create_clone_machine_script" {
  type    = bool
  default = false
}

variable "clone_script_name" {
  type    = string
  default = "clone-machine.sh"
}
