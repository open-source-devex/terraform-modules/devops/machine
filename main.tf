resource "gitlab_group" "group" {
  parent_id = var.parent_group_id

  name = var.group_name
  path = var.group_path != "" ? var.group_path : var.group_name

  description = var.group_description

  visibility_level = var.gitlab_visibility_level
}
