#!/usr/bin/env sh

set -e

TARGET_DIR=$${1}

if [[ -z "$${TARGET_DIR}" ]]; then
    echo "==# Need target dir as first argument"
    exit 1
fi

echo "==> Cloning machine ${machine_name} into $${TARGET_DIR}"

cd $${TARGET_DIR}

${git_clone_repos}
